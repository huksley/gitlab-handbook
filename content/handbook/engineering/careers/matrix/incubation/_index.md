---
title: Incubation Engineering Department Career Framework
---

{{% include "includes/engineering-careers/career-matrix-nav.md" %}}

The Incubation Engineering Department career tracks are visualised on the [Engineering career development landing page](/handbook/engineering/career-development/#incubation-engineering-department).

These are the expected competencies of team members at GitLab by Job Title and job grade.

- Incubation Engineer
    - [Intermediate](/handbook/engineering/career-development/matrix/engineering/development/incubation/intermediate/)
    - [Senior](/handbook/engineering/career-development/matrix/engineering/development/incubation/senior/)
    - [Staff](/handbook/engineering/career-development/matrix/engineering/development/incubation/staff/)
    - [Principal](/handbook/engineering/career-development/matrix/engineering/development/incubation/principal/)
- Director of Incubation Engineering
- Vice President of Incubation Engineering
