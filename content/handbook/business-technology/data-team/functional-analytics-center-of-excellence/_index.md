---

title: "Functional Analytics Center of Excellence"
description: "The FACE is a cross-functional group of functional analytics teams that aim to make our teams more efficient by solving and validating shared data questions which results in cohesive measurement approaches across teams."
---


## Overview
**Functional Analytics Center of Excellence** pages are available in our [Internal GitLab Handbook](https://internal.gitlab.com/handbook/enterprise-data/functional-analytics-center-of-excellence). 





